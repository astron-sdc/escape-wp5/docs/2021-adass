\documentclass[11pt,twoside]{article}

% Do NOT use ANY packages other than asp2014. 
\usepackage{asp2014}

\aspSuppressVolSlug
\resetcounters

% References must all use BibTeX entries in a .bibfile.
% References must be cited in the text using \citet{} or \citep{}.
% Do not use \cite{}.
% See ManuscriptInstructions.pdf for more details
\bibliographystyle{asp2014}

% The ``markboth'' line sets up the running heads for the paper.
% 1 author: "Surname"
% 2 authors: "Surname1 and Surname2"
% 3 authors: "Surname1, Surname2, and Surname3"
% >3 authors: "Surname1 et al."
% Replace ``Short Title'' with the actual paper title, shortened if necessary.
% Use mixed case type for the shortened title
% Ensure shortened title does not cause an overfull hbox LaTeX error
% See ASPmanual2010.pdf 2.1.4  and ManuscriptInstructions.pdf for more details
\markboth{Swinbank et al.}{ESAP}

\begin{document}

\title{ESAP: The ESCAPE ESFRI Science Analysis Platform}

% Note the position of the comma between the author name and the 
% affiliation number.
% Authors surnames should come after first names or initials, eg John Smith, or J. Smith.
% Author names should be separated by commas.
% The final author should be preceded by "and".
% Affiliations should not be repeated across multiple \affil commands. If several
% authors share an affiliation this should be in a single \affil which can then
% be referenced for several author names. If only one affiliation, no footnotes are needed.
% See ManuscriptInstructions.pdf and ASP's manual2010.pdf 3.1.4 for more details

%\author{Sample~Author1,$^1$ Sample~Author2,$^2$ and Sample~Author3$^2$}
%\affil{$^1$Institution Name, Institution City, State/Province, Country; \email{AuthorEmail@email.edu}}
%\affil{$^2$Institution Name, Institution City, State/Province, Country}

\author{John~D.~Swinbank$^1$, Sara~Bertocco$^2$, Stefano~A.~Russo$^2$, and Susana S\'anchez-Exp\'osito$^3$}
\affil{$^1$ASTRON, 7991 PD Dwingeloo, The Netherlands; \email{swinbank@astron.nl}}
\affil{$^2$INAF, Osservatorio Astronomico di Trieste, 34143 Trieste, Italy}
\affil{$^3$Instituto de Astrof\'isica de Andaluc\'ia (CSIC), 18008 Granada, Spain}

% This section is for ADS Processing.  There must be one line per author. paperauthor has 9 arguments.
\paperauthor{John~D.~Swinbank}{swinbank@astron.nl}{https://orcid.org/0000-0001-9445-1846}{ASTRON}{}{Dwingeloo}{Drenthe}{3571\,KD}{The Netherlands}
\paperauthor{Sara~Bertocco}{sara.bertocco@inaf.it}{ orcid.org/0000-0003-2386-623X}{INAF}{Osservatorio Astronomico di Trieste}{Trieste}{}{34143}{Italy}
\paperauthor{Stefano~Alberto~Russo}{stefano.russo@inaf.it}{ orcid.org/0000-0003-2386-623X}{INAF}{Osservatorio Astronomico di Trieste}{Trieste}{}{34143}{Italy}
\paperauthor{Susana S\'anchez-Exp\'osito}{sse@iaa.es}{ https://orcid.org/0000-0002-7510-7633}{Consejo Superior de Investigaciones Cient\'ificas}{Instituto de Astrof\'isica de Andaluc\'ia}{Granada}{}{18008}{Spain}

% There should be one \aindex line (commented out) for each author. These are used to
% build up the author index for the Proceedings. The surname must come first, followed by
% initials. Note the use of ~ before each initial to control spacing.
% The \author entries (see above) have surname last. These \aindex entries have
% surname first.
% The Aindex.py command willl create them for you after you have constructed the \author
% The first entry should be the first author, for bold-facing the author index page-reference

%\aindex{Swinbank,~J.~D.}

\begin{abstract}
ESAP, the ESFRI Science Analysis Platform, is being developed within the ESCAPE project to provide a flexible toolkit for constructing science platforms.
It provides capabilities to quickly unite a range of data access and analysis services --- in particular those developed by the ESCAPE project --- behind a single, consistent but customizable, user interface.
This paper provides a brief overview of ESAP's aims, its architecture, and its current and expected future capabilities.
\end{abstract}
%\ssindex{projects!ESCAPE}

\section{ESCAPE and ESAP}

%\ssindex{organisations!European Strategy Forum on Research Infrastructures (ESFRI)}
The ESCAPE project\footnote{\url{https://www.projectescape.eu}} brings together the astronomy, astroparticle, and particle physics communities to address fundamental challenges in data-driven research, inspired by the goals and needs of major European research infrastructures, or ESFRIs.

%\ssindex{FAIR}
%\ssindex{projects!EOSC}
ESCAPE aims to produce versatile solutions to support the implementation of the European Open Science Cloud by fostering a multi-disciplinary environment, including open data management according to FAIR principles.
Our goal is to enable interoperability between facilities, encourage cross-fertilization, and develop joint multiwavelength and multimessenger data processing and analysis capabilities.
We are addressing this goal by developing a range of services, including data infrastructure, a software repository, virtual observatory integration, and a citizen science programme.

%\ssindex{science portal}
ESAP, the ESFRI Science Analysis Platform, is under development by ESCAPE's Work Package 5, to facilitate access to the full range of ESCAPE services.
ESAP aims to provide a comfortable and consistent gateway to the full range of project-provided functionality, while being customizable and adaptable to the needs of particular science goals or research infrastructures.

\section{The ESAP Mission}

\articlefigure{X0-010_f1.eps}{fig:esap-interfaces}{ESAP provides a single, consistent, interface and point of access to a variety services drawn from a range of providers.
Links to the other work packages within ESCAPE are indicated.}

ESAP will help users engage with the services provided by the other ESCAPE work packages --- and with services sourced from elsewhere -- by providing:

\begin{itemize}

\item{Data discovery and retrieval from a range of archives and data repositories;}
\item{Exploration and discovery of relevant tools within the ESCAPE software repository;}
\item{Access to a range of compute and analysis services provided both by project partners and by other facilities;}
\item{Orchestration of data, services, and software to help users create and access research environments that meet their particular needs.}

\end{itemize}
This model is shown schematically in Figure \ref{fig:esap-interfaces}, which illustrates the range of services which ESAP can help the user access.

Our aim is to meet the needs of the widest possible range of scientists, using a variety of different research infrastructures.
To make this possible, we do not envision ESAP as being operated as a single integrated platform to which all researchers must adapt.
Instead, ESAP provides a ``toolkit'', from which various communities and research infrastructures can assemble a customized analysis platform tailored to their specific needs.
We envision instances of ESAP being deployed at a variety of scales, from providing services to just a few users within a small project, to supporting major pieces of infrastructure.

\section{Architecture}

\articlefigure{X0-010_f2.eps}{fig:esap-arch}{A conceptual view of the ESAP architecture, showing a range of possible service integrations.}

%\ssindex{computer languages!Python}
%\ssindex{software!tools!Django}
%\ssindex{web!development tools!React}
%\ssindex{computing!architecture!REST}
ESAP is a web application, implemented in Python, Django, and React.\footnote{\url{https://www.python.org}; \url{https://www.djangoproject.com}; \url{https://reactjs.org}}
It is designed around the \emph{API Gateway}, which brokers requests across a range of independent services, and a web-based \emph{user interface}.
This architecture is shown schematically in Figure \ref{fig:esap-arch}.

The API Gateway communicates with external services using REST APIs.
New service integrations can easily be added by writing ``plug-in'' modules which extend the Gateway's capabilities.
The ESCAPE team is writing a number of service integration plugins which will be supplied with ESAP; we hope that the wider community will contribute many more.

Internally, ESAP uses the model of a ``shopping basket'' to help users manage their data and workflows.
As they interact with external services, they can select data or other products of interest and add them to their basket.
They can edit the contents of their basket from within the ESAP interface itself, and take the contents of their basket with them as they move to analysis services.
While the basket is designed to be flexible in terms of the types of data it stores, it is normally used to store references to bulk data products, rather than the products themselves; this enables us to minimize unnecessary bulk data transport between services.

\section{Capabilities}

ESAP is still very much under development at time of writing, and its range of capabilities continues to expand.
Some highlights of current capabilities include:

\begin{itemize}

%\ssindex{protocols!SAMP}
%\ssindex{instruments!individual!APERTIF}
\item{Access to a variety of bespoke archives, including Apertif and Zooniverse;\footnote{\url{http://www.apertif.nl/}; \url{https://www.zooniverse.org/}}}
\item{Powerful tools for searching the Virtual Observatory through IVOA-compliant interfaces, and for integrating with VO tools through SAMP, the Simple Application Messaging Protocol;}
\item{The capability to search and discover data on the Rucio\footnote{\url{https://rucio.cern.ch}}-based ESCAPE data lake;}
\item{The ability to locate software in the ESCAPE project software repository, and to stage that software to BinderHub\footnote{\url{https://binderhub.readthedocs.io/}}-based interactive data analysis services;}
\item{Bi-directional data transfer between the ESAP ``shopping basket'' and Python-based analysis services;}
\item{Integration with the ESCAPE project's Identity \& Access Management system.}

\end{itemize}
Future work is expected to focus on:

\begin{itemize}

\item{Integration with batch computing services;}
\item{A managed database system;}
\item{Closer interaction between the analysis environment and the Rucio data lake, building on ``data lake as a service'' technology developed in ESCAPE Work Package 2;}
\item{Tighter integration with the Virtual Observatory, through ESCAPE WP4, and the ESCAPE software repository, developed by WP3.}

\end{itemize}

\section{Downloads and Further Information}

ESAP is available under the terms of the Apache license, version 2.0, from \url{https://git.astron.nl/astron-sdc/esap-api-gateway} and \url{https://git.astron.nl/astron-sdc/esap-gui}.

A test system is available at \url{https://sdc-dev.astron.nl/esap-gui}.
ESAP is currently under heavy development: stability and uptime are not guaranteed, but your feedback is always welcome.

\acknowledgements ESCAPE European Science Cluster of Astronomy \& Particle physics ESFRI research Infrastructures has received funding from the European Union's Horizon 2020 research and innovation programme under the Grant Agreement n$^\mathrm{o}$ 824064.

% if we have space left, we might add a conference photograph here. Leave commented for now.
% \bookpartphoto[width=1.0\textwidth]{foobar.eps}{FooBar Photo (Photo: Any Photographer)}

\end{document}
